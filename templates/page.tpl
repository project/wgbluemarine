 * wgSmarty page template
 * Theme: wgBluemarine
 * Note: be careful not to leave any additional lines before <!DOCTYPE
 *}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="{$language}" xml:lang="{$language}">

<head>
  <title>{$head_title}</title>
  {$head}
  {$styles}
  <script type="text/javascript"> </script>
</head>

<body{$onload_attributes}>

<table border="0" cellpadding="0" cellspacing="0" id="header">
  <tr>

{* SITE LOGO/NAME *********************************************************** *}
    <td id="logo">
{if $logo}<a href="./" title="Home"><img src="{$logo}" alt="Home" /></a>{/if}
{if $site_name}<h1 class='site-name'><a href="./" title="Home">{$site_name}</a></h1>{/if}
{if $site_slogan}<div class='site-slogan'>{$site_slogan}</div>{/if}
    </td>

	<td id="menu">

{* PRIMARY LINKS ************************************************************ *}
{if $links.secondary}
      <div id="secondary">
  {foreach item=link from=$links.secondary name=loop}
        {$link}{if !$smarty.foreach.loop.last} | {/if}
  {/foreach}
      </div>
{/if}

{* SECONDARY LINKS ********************************************************** *}
{if $links.primary}
      <div id="primary">
  {foreach item=link from=$links.primary name=loop}
        {$link}{if !$smarty.foreach.loop.last} | {/if}
  {/foreach}
      </div>
{/if}

{* SEARCH BOX *************************************************************** *}
{if $search_box}
      <form action="{$search_url}" method="post">
        <div id="search">
          <input class="form-text" type="text" size="15" value="" name="edit[keys]" alt="{$search_description}" />
          <input class="form-submit" type="submit" value="{$search_button_text}" />
        </div>
      </form>
{/if}

	</td>

  </tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" id="content">
  <tr>

{* LEFT BLOCKS ************************************************************** *}
{if $blocks.1}
    <td id="sidebar-left">
      {sidebar from=$blocks.1}
    </td>
{/if}

	<td valign="top">

{* MISSION ****************************************************************** *}
{if $mission}<div id="mission">{$mission}</div>{/if}

	  <div id="main">

{* BREADCRUMBS ************************************************************** *}
        {$breadcrumb}

{* TITLE ******************************************************************** *}
{if $title}<h1 class="title">{$title}</h1>{/if}

{* TABS ********************************************************************* *}
{if $tabs} <div class="tabs">{$tabs}</div>{/if}

{* HELP ********************************************************************* *}
{if $help} <div id="help">{$help}</div>{/if}

{* MAIN CONTENT ************************************************************* *}
{$content}

      </div>
    </td>

{* RIGHT BLOCKS ************************************************************* *}
{if $blocks.2}
    <td id="sidebar-right">
      {sidebar from=$blocks.2}
    </td>
{/if}

  </tr>
</table>

{* FOOTER MESSAGE *********************************************************** *}
<div id="footer">
  {$footer_message}
</div>

{* MODULE FOOTERS *********************************************************** *}
{$closure}
</body>
</html>
